#!/usr/bin/env python3

# ------------------------------------------------------------------
# assignments/assignment5.py
# Fares Fraij
# ------------------------------------------------------------------

#-----------
# imports
#-----------

from flask import Flask, render_template, request, redirect, url_for, jsonify
import random
import json

app = Flask(__name__)

# Dictionary of books
books = [{'title': 'Software Engineering', 'id': '1'}, \
		 {'title': 'Algorithm Design', 'id':'2'}, \
		 {'title': 'Python', 'id':'3'}]

@app.route('/book/JSON/')
def bookJSON():
    return json.dumps(books)

@app.route('/')
@app.route('/book/')
def showBook():
    return render_template('showBooks.html', books = books)
	
@app.route('/book/new/')
def renderNewBook():
    return render_template('newBook.html')

@app.route('/book/new/render', methods=['GET','POST'])
def newBook():

    if request.method == "POST":
        name = request.form['name']

        index = 1;
        inserted = False;

        for book in books:
            if index < int(book['id']):
                newBook = {'title': name, 'id': index}
                books.insert(index - 1, newBook)
                inserted = True;
            index += 1
        
        if not inserted:
            newBook = {'title': name, 'id': index}
            books.append(newBook)

        return redirect(url_for('showBook'))

    else:
        return redirect(url_for('showBook'))

@app.route('/book/<int:book_id>/edit/', methods=['GET','POST'])
def editBook(book_id):
    if request.method == "POST":
        bookToEdit = {}
        for book in books:
            if int(book['id']) == book_id:
                bookToEdit = book
        newName = request.form['name']
        bookToEdit['title'] = newName
        return redirect(url_for('showBook'))
    else:
        return redirect(url_for('showBook'))

@app.route('/book/<int:book_id>/edit/render')
def renderEditBook(book_id):
    bookToRender = {}
    for book in books:
        if int(book['id']) == book_id:
            bookToRender = book
    return render_template('editBook.html', book = bookToRender)

	
@app.route('/book/<int:book_id>/delete/', methods = ['GET', 'POST'])
def deleteBook(book_id):
    if request.method == "POST":
        bookToDelete = {}
        for book in books:
            if int(book['id']) == book_id:
                books.remove(book)
                break
        return redirect(url_for('showBook'))
    else:
        return redirect(url_for('showBook'))


@app.route('/book/<int:book_id>/delete/render')
def renderDeleteBook(book_id):
    bookToRender = {}
    for book in books:
        if int(book['id']) == book_id:
            bookToRender = book
    return render_template('deleteBook.html', book = bookToRender)

if __name__ == '__main__':
	app.debug = True
	app.run(host = '127.0.0.1', port = 5000)